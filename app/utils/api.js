import axios from 'axios';

class Axios {
  constructor(){
    this.params = 'RGAPI-a5b0575a-3c99-401e-957f-451060f72b5a';
  }

  getBestUser(region) {
    const encodedUri = window.encodeURI(`https://${region}.api.riotgames.com/api/lol/${region}/v2.5/league/challenger?type=RANKED_SOLO_5x5&api_key=${this.params}`);
    return axios.get(encodedUri);
  }

  getSumonerInfo(region, sumId) {
    switch(region){
      case 'EUW':
        region = 'EUW1';
        break;
      case 'EUN':
        region = 'EUN1';
        break;
      case 'NA':
        region = 'NA1';
        break;
      case 'JP':
        region = 'JP1';
        break;
      case 'BR':
        region = 'BR1';
        break;
      case 'TR':
        region = 'TR1';
        break;
      case 'OC':
        region = 'OC1';
        break;
      case 'LA':
        region = 'LA1';
        break;
      default:
        return region;
    }
    const encodedUri = `https://${region}.api.riotgames.com/lol/summoner/v3/summoners/${sumId}?api_key=${this.params}&limit=5`;
    return axios.get(encodedUri);
  }

  handleError(error){
    console.warn(error);
    return false;
  }

  getUserData(region) {

  }
}

const api = new Axios();

export default api;
