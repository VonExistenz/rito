import React from 'react';
import api from '../utils/api';
import Loading from './Loading';

const SelectRegion = (props) => {
  const regions = ['RU', 'LAN', 'EUNE', 'NA', 'TR',
   'JP', 'KR', 'BR', 'OCE', 'EUW', 'LAS'];

  return(
    <ul className='list-style'>
    {regions.map((items) => {
      let handleSelect = items === props.selectedRegion ? {color: '#d0021B'} : null;
      let clickFunc = props.onSelect.bind(null, items);
        return(
          <li key={items}
            style={handleSelect}
            onClick={clickFunc}>
              {items}
          </li>
        )
      })}
    </ul>
  )
}

const ShowBestPlayer = (props) => {
  return(
    <ul className='popular-list'>
      {props.players.map((items, index) => {
        return(
          <li key={items.playerOrTeamId} className='popular-item'>
            <div className='popular-rank'>
              #{index + 1}
              <h3>{items.playerOrTeamName}</h3>
              <span>Wins: {items.wins}</span>
              <br />
              <button className='button' id={items.playerOrTeamId}>View More</button>
            </div>
          </li>
        )
      })}
    </ul>
  )
}

class Home extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      selectedRegion: 'EUW',
      loading: true,
      type: null,
      players: null
    }

    this.updateRegion = this.updateRegion.bind(this);
  }

  componentDidMount() {
      // AJAX Request
      this.updateRegion(this.state.selectedRegion);
  }

  getBestPlayer(region){
    let self = this;
    return api.getBestUser(region)
      .then((response) => {
        let datas = response.data.entries;
        self.setState(() => {
          return {
            players: datas,
            loading: false
          }
        });
      })
      .catch((err) => {api.handleError(err)});
  }

  updateRegion(region){
    this.setState(() => {
      return {
        selectedRegion: region,
        loading: true
      }
    })
    this.getBestPlayer(region);

  }

  render(){
    return(
      <div className='homepage'>
        <SelectRegion
          selectedRegion ={this.state.selectedRegion}
          onSelect ={this.updateRegion}
        />
        {this.state.players && !this.state.loading &&
          <ShowBestPlayer players={this.state.players} />
        }
        {this.state.loading &&
          <Loading />
        }
      </div>
    )
  }
}

export default Home;
