import React from 'react';
import * as ReactRouter from 'react-router-dom';
const Router = ReactRouter.BrowserRouter;
const Route = ReactRouter.Route;
const Switch = ReactRouter.Switch;
import Home from './Home';
import Popular from './Popular';
import Nav from './Nav';

class App extends React.Component{
    render(){
        return (
          <Router>
            <div className='container'>
              <Nav />
              <Switch>
                <Route exact path='/' component={Home} />
                <Route exact path='/popular' component={Popular} />
                <Route render={() => <p>Not found</p>} />
              </Switch>
            </div>
          </Router>
        )
    }
}

export default App;
